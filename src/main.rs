#![allow(unused_imports)]

use actix::{
    dev::channel::{channel, AddressSender},
    fut,
    prelude::*,
    utils::TimerFunc,
    Actor, Addr, AsyncContext, Context, Handler, Message, MessageResult, SpawnHandle,
    StreamHandler,
};
use actix_files::NamedFile;
use actix_identity::{CookieIdentityPolicy, Identity, IdentityService};
use actix_web::{
    error, get, post, web, App, Error, HttpRequest, HttpResponse, HttpServer, Responder,
};
use actix_web_actors::ws::{self, WebsocketContext};
use log::{debug, error, info, warn};
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    path::PathBuf,
    sync::{Arc, Mutex},
    time::Duration,
};
use tera::Tera;

mod configuration;
mod game;

use game::{Arena, ArenaAddr, MyWs, Pos2d, Snake};

const PAGECLASS_KEY: &'static str = "pageclass";

async fn ws_start(
    arena: web::Data<ArenaAddr>,
    stats: web::Data<Addr<game::Stats>>,
    req: HttpRequest,
    stream: web::Payload,
) -> Result<HttpResponse, Error> {
    let arena: &ArenaAddr = arena.get_ref();
    let resp = ws::start(MyWs::new(arena.clone()), &req, stream);
    if let Err(e) = &resp {
        error!("/ws/ -> {:?}", e);
        stats.do_send(game::StatsNoWebsocketUpgrade);
    } else {
        stats.do_send(game::StatsWebsocketUpgrade)
    }
    resp
}

#[get("/")]
async fn index(id: Identity, tmpl: web::Data<tera::Tera>) -> Result<HttpResponse, Error> {
    let (mut ctx, _user) = default_tera_context(&id);
    ctx.insert(PAGECLASS_KEY, "index");
    let s = tmpl.render("index.html", &ctx).map_err(|e| {
        warn!("index: {:?}", e);
        error::ErrorInternalServerError("Template error index.html")
    })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[get("/play")]
async fn play(id: Identity, tmpl: web::Data<tera::Tera>) -> Result<HttpResponse, Error> {
    let (ctx, _user) = default_tera_context(&id);
    let s = tmpl.render("play.html", &ctx).map_err(|e| {
        warn!("index: {:?}", e);
        error::ErrorInternalServerError("Template error play.html")
    })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_web=info,multiplayer_snake=debug");
    env_logger::init();

    let config = configuration::restore_or_default().expect("unable to restore configuration");
    let private_key = config.get_private_key();
    let bind_addr = get_prod_bind_addr().unwrap_or("[::]:8080".to_string());
    game::debug_messages();

    info!("binding to {:?}", bind_addr);

    let config = Arc::new(Mutex::new(config));

    let (height, width, n_snakes) = (8, 12, 2);

    let (arena, statistics) = Arena::new(width, height, n_snakes);

    HttpServer::new(move || {
        let tera = Tera::new("templates/**/*.html").expect("unable to load tera templates");

        App::new()
            .data(tera)
            .data(arena.clone())
            .data(config.clone())
            .data(statistics.clone())
            .wrap(IdentityService::new(
                // <- create identity middleware
                CookieIdentityPolicy::new(&private_key) // <- create cookie identity policy
                    .name("auth-cookie")
                    .secure(true),
                //.secure(true),
            ))
            .service(index)
            .service(play)
            .service(
                actix_files::Files::new("/static", "./static/")
                    .use_last_modified(true)
                    .use_etag(true)
                    .show_files_listing(),
            )
            .service(web::resource("/me/login").to(login))
            .service(web::resource("/me/logout").to(logout))
            .service(web::resource("/me/identicon").route(web::get().to(identicon)))
            .service(web::resource("/admin").to(admin))
            .service(web::resource("/admin/update").to(admin_update))
            .service(web::resource("/admin/metrics").to(metrics))
            .service(web::resource("/rc3").route(web::get().to(rc3)))
            .route("/ws/", web::get().to(ws_start))
    })
    .bind(bind_addr)?
    .run()
    .await
}

#[derive(Serialize, Deserialize)]
pub struct LoginParams {
    userkey: String,
}

async fn login(
    id: Identity,
    tmpl: web::Data<tera::Tera>,
    users: configuration::WebConfig,
    params: Option<web::Form<LoginParams>>,
    query: web::Query<HashMap<String, String>>,
) -> Result<HttpResponse, Error> {
    let next = match query.get("next") {
        Some(n) => n,
        None => "/",
    };

    let (mut ctx, _user) = default_tera_context(&id);
    let mut messages = vec![];

    if let Some(params) = params {
        let userkey = params.userkey.trim();
        let mut users = users.lock().expect("unable to lock users");

        if let Some(_user_data) = users.get_user_data(userkey) {
            id.remember(userkey.to_string());
            info!("login accepted: {:?}", id.identity());
            return Ok(HttpResponse::Found().header("location", next).finish());
        } else {
            ctx.insert("userkey", userkey);
            messages.push("Unknown userkey");
        }
    }

    debug!("rejecte atempt");
    ctx.insert("messages", &messages);
    ctx.insert("next", next);

    let s = tmpl.render("sign_in_first.html", &ctx).map_err(|e| {
        warn!("me::login: {:?}", e);
        error::ErrorInternalServerError("Template error sign_in_first.html")
    })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

async fn logout(id: Identity) -> HttpResponse {
    id.forget();
    HttpResponse::Found().header("location", "/").finish()
}

pub async fn identicon(id: Identity) -> HttpResponse {
    let user = new_user_context(&id);

    let border = 16;
    let size = 8;
    let scale = 128;
    let background_color = //(240, 220, 200)
    (0xff,0xa8,0x72);

    let identicon = identicon_rs::Identicon::new(&user.userkey)
        .border(border)
        .background_color(background_color)
        .size(size)
        .unwrap()
        .scale(scale)
        .unwrap();
    let file = identicon.export_png_data().unwrap();

    HttpResponse::Ok().content_type("image/png").body(file)
}

pub async fn rc3(id: Identity, tmpl: web::Data<tera::Tera>) -> Result<HttpResponse, Error> {
    let (ctx, _user) = default_tera_context(&id);

    let s = tmpl.render("rc3.html", &ctx).map_err(|e| {
        warn!("rc3: {:?}", e);
        error::ErrorInternalServerError("Template error rc3.html")
    })?;

    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

#[derive(Serialize, Deserialize)]
pub struct UserContext {
    authenticated: bool,
    userkey: String,
}
pub fn new_user_context(id: &Identity) -> UserContext {
    let (authenticated, userkey) = match id.identity() {
        Some(userkey) => (true, userkey),
        None => (false, "Anonymous".to_owned()),
    };
    UserContext {
        authenticated,
        userkey,
    }
}

pub fn default_tera_context(id: &Identity) -> (tera::Context, UserContext) {
    let mut ctx = tera::Context::new();
    let user = new_user_context(&id);
    ctx.insert("user", &user);
    ctx.insert(PAGECLASS_KEY, "");
    (ctx, user)
}

fn get_prod_bind_addr() -> std::io::Result<String> {
    let contents = std::fs::read_to_string("./backend_url.txt")?;

    Ok(extract_bind_addr(contents))
}
fn extract_bind_addr<S: AsRef<str>>(contents: S) -> String {
    contents
        .as_ref()
        .trim()
        .trim_start_matches("http://")
        .trim_end_matches("/")
        .to_string()
}

#[test]
fn test_get_prod_bind_addr() {
    assert_eq!("[::1]:5000", extract_bind_addr("http://[::1]:5000/\n"));
}

// ------------------ ADMIN

async fn admin(
    id: Identity,
    tmpl: web::Data<tera::Tera>,
    //users: configuration::WebConfig,
    //params: Option<web::Form<LoginParams>>,
    //query: web::Query<HashMap<String, String>>,
    arena: web::Data<ArenaAddr>,
) -> Result<HttpResponse, Error> {
    let (mut ctx, user) = default_tera_context(&id);
    if user.authenticated == false {
        return Ok(HttpResponse::Found()
            .header("location", "/me/login?next=/admin")
            .finish());
    }

    let state = arena
        .send(game::GetArenaConfig)
        .await
        .expect("unable to GetArenaConfig for /admin");

    ctx.insert("width", &state.width);
    ctx.insert("height", &state.height);
    ctx.insert("n_snakes", &state.n_snakes);

    let s = tmpl.render("admin.html", &ctx).map_err(|e| {
        warn!("admin: {:?}", e);
        error::ErrorInternalServerError("Template error admin.html")
    })?;
    Ok(HttpResponse::Ok().content_type("text/html").body(s))
}

//#[post("/admin/update")]
async fn admin_update(
    id: Identity,
    arena: web::Data<ArenaAddr>,
    new_config: web::Json<game::UpdateArenaConfig>,
) -> impl Responder {
    let (_ctx, user) = default_tera_context(&id);
    if user.authenticated == false {
        return Ok(HttpResponse::Found()
            .header("location", "/me/login?next=/admin")
            .finish());
    }

    let new_config: &game::UpdateArenaConfig = &*new_config;

    let response = arena
        .send(new_config.clone())
        .await
        .expect("unable to update arena config");

    HttpResponse::Ok().json(response).await
}

async fn metrics(
    id: Identity,
    //tmpl: web::Data<tera::Tera>,
    users: configuration::WebConfig,
    //params: Option<web::Form<LoginParams>>,
    query: web::Query<HashMap<String, String>>,
    stats: web::Data<Addr<game::Stats>>,
    //arena: web::Data<ArenaAddr>,
) -> Result<HttpResponse, Error> {
    let validate_userkey = |userkey: &String| {
        // debug!("validate_userkey({})", userkey);
        let mut users = users.lock().expect("metrics: unable to lock WebConfig");

        let is_admin = users.get_user_data(userkey);

        is_admin.map(|_| true)
    };

    let token_authenticated = query.get("key").and_then(validate_userkey).is_some();
    // debug!("/metrics? <- {:?} <- {:?}", query, token_authenticated,);

    let (_ctx, user) = default_tera_context(&id);
    if user.authenticated == false && token_authenticated == false {
        return Ok(HttpResponse::Found()
            .header("location", "/me/login?next=/admin/metrics")
            .finish());
    }

    let state: game::Metrics = stats
        .send(game::GetMetrics)
        .await
        .expect("unable to GetMetrics for /admin/metrics");

    return Ok(HttpResponse::Ok().content_type("text/plain").body(state.0));
}
