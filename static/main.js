'use strict';

window.onerror = function (msg, url, line) {
    alert("Message : " + msg
        + "\nURL : " + url
        + "\nLine number : " + line
        + "\nWS URL: " + ((location.protocol === 'http:' ? 'ws://' : 'wss://') + location.host + '/ws/') );
}

const Main = (function () {
    const current_players = document.querySelector('#current_players')
        , ping_time = document.querySelector('#ping_time');
    const modal_explanation = document.querySelector('#modal-explanation');
    
    setTimeout(function() {
        if (ws.readyState !== WebSocket.OPEN) {
            current_players.innerText = 'Unable to connect! Please try to reload the page and check the system requirements';
        }
    }, 5*1000);

    // Show modal info
    modal_explanation.classList.remove('hidden');

    // Workaround Temporal Dead Zone (:
    modal_explanation.querySelector('#setup-rotation')
        .addEventListener('click', mk_setup_buttons(Buttons.CONTROLL_MODE_ROTATION));
    modal_explanation.querySelector('#setup-d-pad')
        .addEventListener('click', mk_setup_buttons(Buttons.CONTROLL_MODE_D_PAD));

    let ws = new WebSocket((location.protocol === 'http:' ? 'ws://' : 'wss://') + location.host + '/ws/');


    ws.onopen = function (event) {
        //ws.send('Hello Server!');
        current_players.innerText = 'Connected awaiting server input';
    };

    ws.onmessage = function (event) {
        try {
            const command = JSON.parse(event.data);
            for (const cmd in command) {
                //console.log(['cmd', cmd, command[cmd]]);
                switch (cmd) {
                    case "UpdateSnakes":
                        Buttons.msg(cmd, command[cmd]);
                    case "UpdateFieldInfo":
                        Renderer.msg(cmd, command[cmd]);
                        break;
                        
                    case "UpdatePlayerCount":
                        current_players.innerText = `Currently active players: ${command[cmd]}`;
                        break;


                    case "UpdateControllColor":
                        Buttons.update_controll_color(command[cmd]);
                        Renderer.msg(cmd, command[cmd]);
                        break;

                    case "PingTime":
                        const c = command[cmd];
                        let ms = 'unknown';
                        if (c.Ping) {
                            ms = `${c.Ping.ms}ms`;
                        } else if (c.NetworkTooSlow) {
                            ms = `<b class="warning">${c.NetworkTooSlow.ms}ms</b>`
                        } else {
                            throw `unimplemented PingTime: ${c}`;
                        }
                        ping_time.innerHTML = `Ping: ${ms}`;
                        break;
                
                    default:
                        console.error(["unimplemented command", cmd]);
                        break;
                }
            }
        } catch (error) {
            console.error(["invalid command from server", error, event.data]);
        }
    };

    ws.onclose = function (event) {
        console.log('Lost connection');
        current_players.innerText = 'Lost connection';

        check_online();
        async function check_online() {
            try { 
                const a = await fetch('/ws/');
                if (a.status == 400) {
                    console.log("reloading...");
                    location.reload(true);
                } else {
                    requestAnimationFrame(_ => {
                        setTimeout(check_online, 1000);
                    });
                }
            } catch (error) {
                requestAnimationFrame(_ => {
                    setTimeout(check_online, 1000);
                });
            }
        }
    };

    function mk_setup_buttons(style) {
        return function () {
            Buttons.setup(style);
            hide_info_box();
        }
    }

    function hide_info_box() {
        modal_explanation.classList.add('hidden');
    }

    return {
        send: function (data) {
            const s = JSON.stringify(data);
            if (ws.readyState === WebSocket.OPEN) {
                ws.send(s);
                hide_info_box();
            } else {
                console.error(['Main::send::unable_to_send', data]);
            }
        },
        close: function () {
            ws.close();
        },
        hide_info_box: hide_info_box,
    }

})();
